import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRouter } from './routes';
import { AuthService } from './auth.service';
import { TodosService } from './todos.service';
import { HttpClient } from './http-client.service';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TodosComponent } from './todos/todos.component'; 

@NgModule({
    declarations: [AppComponent, LoginComponent, DashboardComponent, TodosComponent],
    imports: [BrowserModule, HttpModule, RouterModule, FormsModule, AppRouter],
    providers: [AuthService, TodosService, HttpClient],
    bootstrap: [AppComponent]
})

export class AppModule {}
