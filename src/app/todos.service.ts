import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from './http-client.service';

@Injectable()
export class TodosService {
  isAuthenticated: boolean = false;
  constructor(private http: HttpClient, private router: Router) { }
  getTodos() {
    return new Promise((resolve) => {
      this.http.get('todos').subscribe((data) => {
        if(data.json().success) {
          resolve(data);
        }
      })
    })
  }
  addTodo(todo) {
    return new Promise((resolve) => {
      this.http.post('todos', todo).subscribe((data) => {
        if(data.json().success) {
          resolve(data);
        }
      })
    })
  }
  editTodo(todo) {
    return new Promise((resolve) => {
      this.http.put('todos/'+todo.id, todo).subscribe((data) => {
        if(data.json().success) {
          resolve(data);
        }
      })
    })
  }
  deleteTodo(todo) {
    return new Promise((resolve) => {
      this.http.delete('todos/'+todo.id).subscribe((data) => {
        if(data.json().success) {
          resolve(data);
        }
      })
    })
  }
}
