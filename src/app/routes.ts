import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TodosComponent} from './todos/todos.component';


export const appRoutes: Routes = [
    {path: '', component:DashboardComponent},
    {path: 'login', component:LoginComponent},
    {path: 'dashboard', component:DashboardComponent},
    {path: 'todos', component:TodosComponent},

]

export const AppRouter = RouterModule.forRoot(appRoutes);
