import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';
import {Router} from '@angular/router';
import { HttpClient } from './http-client.service';

@Injectable()
export class AuthService {
  isAuthenticated: boolean = false;
  userId;
  windowHandle;
  ourcode;
  client_id = '1';
  client_secret= 'Pkf6WcoMZ18XrmRwXZayNKUpDIuQi9m66fGvaNMs';
  constructor(private http: HttpClient, private router: Router) { }
  login(usercreds) {
    var creds = 'name=' + usercreds.username + '&password=' + usercreds.password;
    return new Promise((resolve) => {
      this.http.post('authenticate', creds).subscribe((data) => {
          if(data.json().success) {
              this.userId = data.json().userId;
              this.isAuthenticated = true;}
              resolve(this.isAuthenticated);
          }
      )
    });
  }
}
