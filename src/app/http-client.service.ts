import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class HttpClient {
  authUrl: string = 'http://todoapi.local/';
  apiUrl: string = 'http://todoapi.local/api/';
  isAuthenticated: boolean = false;
  headers = new Headers();
  ourcode;
  windowHandle;
  accesstoken;
  client_id = '4';
  client_secret= 'QyaFDBckuZalZKjxNFfp26MmKCaTl6Tvsf9rTT4X';
  constructor(private http: Http) {}
  getAccessToken() {
   var basicheader = btoa(this.client_id + ':' + this.client_secret);
   this.headers.append('Authorization', 'Basic '+ basicheader);
   this.headers.append('Content-Type', 'application/X-www-form-urlencoded');
   var data = {
       'form_params' : {
          'grant_type' : 'password',
          'client_id' : this.client_id,
          'client_secret' : this.client_secret,
          'username' : 'info@sjerd.nl',
          'password' : 'test123',
          'scope' : 'todos'
      }
    }

     this.http.post(this.authUrl+'oauth/token', data, {headers: this.headers}).subscribe((data) => {
       this.accesstoken = data.json().csrfToken;
       this.headers.append('X-CSRF-TOKEN', this.accesstoken);
     })

  }

  get(url) {
    this.getAccessToken();
    return this.http.get(this.apiUrl + url, {
      headers: this.headers
    });
  }

  post(url, data) {
    this.getAccessToken();
    return this.http.post(this.apiUrl + url, data, {
      headers: this.headers
    });
  }

  put(url, data) {
    this.getAccessToken();
    return this.http.put(url, data, {
      headers: this.headers
    });
  }

  delete(url) {
    this.getAccessToken();
    return this.http.delete(url, {
      headers: this.headers
    });
  }
}
