import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../auth.service';
import { TodosService } from '../todos.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {

  constructor(private auth: AuthService, private zone: NgZone, private router: Router, private todoService: TodosService) { }
  todos: Object = {};
  showForm = false;
  todo = {
    'title': '',
    'description': ''
  };
  ngOnInit() {
    let promise = this.todoService.getTodos();
    promise.then((res:any) => {
      this.todos = res.json().data.todos;
    });
  }

  newTodo() {
    this.showForm = true;
  }

  addTodo(todo) {
    let promise = this.todoService.addTodo(todo);
    promise.then((res:any) => {
    });
  }


  cancel() {
    this.showForm = false;
  }

  goback(){
    this.router.navigate(['/dashboard']);
  }
}
